FROM golang:1.13.4-alpine3.10 as build
WORKDIR /go/src/gitlab-rnd.tcsbank.ru/devops/argocd-statesflow
COPY . .
RUN cd ./argocd-statesflow-helper && go build -o /usr/local/bin/argocd-statesflow-helper .

FROM alpine:3.10
ENV KUSTOMIZE_VERSION="v3.4.0"
ENV ARGOCD_VERSION="v1.7.8"
ENV KUSTOMIZE_PROJECT_REV="b522a82"
RUN apk --no-cache add \
        curl \
        wget \
        git \
        openssh-client \
        bash && \
    # ArgoCD
    wget -O /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/${ARGOCD_VERSION}/argocd-linux-amd64 && \
    chmod +x /usr/local/bin/argocd && \
    # Kustomize
    curl -sL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | tar xzf - kustomize && \
    mv kustomize /usr/local/bin/kustomize && \
    kustomize version && \
    # KustomizeProject
    wget -O KustomizeProject https://registry.tcsbank.ru/repository/dist/KustomizeProject/KustomizeProject-${KUSTOMIZE_PROJECT_REV} && \
    chmod +x KustomizeProject && \
    mkdir -p /root/.config/kustomize/plugin/advert.tinkoff.ru/v1/kustomizeproject/ && \
    cp KustomizeProject /usr/local/bin/ && \
    mv KustomizeProject /root/.config/kustomize/plugin/advert.tinkoff.ru/v1/kustomizeproject/ && \
    # k8s-split
    wget -O /usr/local/bin/k8s-split https://registry.tcsbank.ru/repository/dist/k8s-split/k8s-split && \
    chmod +x /usr/local/bin/k8s-split
COPY --from=build /usr/local/bin/argocd-statesflow-helper /usr/local/bin/argocd-statesflow-helper
