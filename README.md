# argocd-statesflow

[![Pipeline](https://gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/badges/master/pipeline.svg)](https://gitlab-rnd.tcsbank.ru/devops/argocd-statesflow)
[![Quality Gate Status](http://cq.tcsbank.ru/api/project_badges/measure?project=argocd-statesflow-helper&metric=alert_status)](http://cq.tcsbank.ru/dashboard?id=argocd-statesflow-helper)
[![Coverage](http://cq.tcsbank.ru/api/project_badges/measure?project=argocd-statesflow-helper&metric=coverage)](http://cq.tcsbank.ru/dashboard?id=argocd-statesflow-helper)

Образ с утилитами для работы с окружениями в ArgoCD, без kubectl и Google Cloud SDK.

## Подключение проекта

### Подготовка репозиториев

Для работы требуется 2 репозитория, один из них должен содержать Kustomize-спецификации, другой – итоговые спецификации – состояния. К оформлению репозитория состояний нет никаких требований, строение же репозитория с Kustomize-спецификациями следует привести к следующему виду:

```
infrastructure/
    namespace/
        kustomization.yaml
        ...
services/
    $CI_PROJECT_NAME/
        resources/
            kustomization.yaml
            ...
        kustomization.yaml (*)
.gitlab-ci.yml
README.md
```

Kustomization-файл отмеченный звездочной является входной точкой для сбора спецификаций для создания нового тестового окружения, в минимальный набор ресурсов этого файла должны входить:

* ресурс `services/$CI_PROJECT_NAME/resources/`, в котором хранятся спецификации необходимые для запуска сервиса, на базе которого производится создание нового окружения;
* ресурс `infrastructure/namespace/`, в котором хранится описание, как минимум, одной сущности – Namespace, в случае PFA там же хранится необходимый набор VirtualService.

### Подготовка CI

Самый простой и действенный способ начать – запустить команду валидации окружения, `argocd-statesflow-helper` сам проверит, что ему не хватает для работы и выдаст список того, что нужно исправить, например, добавить в образ то или иное приложение или добавить какую-то переменную окружения в проект или группу проектов.

Добавьте в `.gitlab-ci.yml` шаг со следующим описанием:

```yaml
validate:
  image: eu.gcr.io/utilities-212509/devops/argocd-statesflow
  stage: review
  script:
    - argocd-statesflow-helper validate
```
Все инструменты, необходимые для работы, уже есть в этом образе, осталось только получить отчет о том, какие переменные следует добавить.

Однако ещё не все проверки перенесены в `argocd-statesflow-helper`, поэтому необходимо самостоятельно убедиться, что у указанного SSH-ключа (который хранится в Vault) есть доступ на чтение репозитория с Kustomize-спецификациями, а так же доступ на запись в репозиторий состояний.

## Ссылки

- https://wiki.tcsbank.ru/display/DIGEDEV/ArgoCD+Statesflow
