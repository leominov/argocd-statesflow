package helper

import (
	"testing"
)

func TestTool_Exists(t *testing.T) {
	tests := []struct {
		tool   *Tool
		exists bool
	}{
		{
			tool: &Tool{
				Path: "go",
			},
			exists: true,
		},
		{
			tool: &Tool{
				Path: "foobar",
			},
			exists: false,
		},
		{
			tool: &Tool{
				Path: "/usr/bin/whoami",
			},
			exists: true,
		},
	}
	for _, test := range tests {
		exists := test.tool.Exists()
		if exists != test.exists {
			t.Errorf("Must be %v, but got %v", test.exists, exists)
		}
	}
}
