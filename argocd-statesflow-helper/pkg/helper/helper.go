package helper

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/google/go-containerregistry/pkg/name"
	"github.com/mitchellh/go-homedir"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/argocd"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/dotssh"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/git"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/k8ssplit"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/kustomize"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/registry"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/vault"
)

const (
	// Set the sync policy (one of: automated, none)
	// https://argoproj.github.io/argo-cd/user-guide/auto_sync/
	appCreateSyncPolicy = "automated"

	// Set self healing when sync is automated
	appCreateSelfHeal = true

	// Set automatic pruning when sync is automated
	appCreateAutoPrune = true
)

type Helper struct {
	// registry клиент для работы с Docker Registry
	registry *registry.Registry

	// vault клиент для работы с HashiCorp Vault
	vault *vault.Vault

	// opts описание всех настроек
	opts *Options

	// statesDir и kustomizeDir содержат пути к временным рабочим каталогам,
	// создаваемым в системе, их создание происходит в функции setupDirs()
	statesDir    string
	kustomizeDir string

	// resourcesDir используется для рендера итоговых спецификация, значение
	// переменной собрано из двух значений: statesDir + "resources", последнее
	// строго зафиксировано и не может быть переопределено где-либо в настройках
	resourcesDir string

	// homeDir используется для настройки окружения, в частности, настройка SSH
	homeDir string

	// imageDigestRE формируется на основе фиксированной строки и имени образа из
	// imageName, который получается на основе STATESFLOW_DOCKER_IMAGE, переданной
	// пользователем, в последующем imageDigestRE будет использована для замены
	// референса образа на полученный из Docker Registry imageSHA
	imageDigestRE *regexp.Regexp
	imageName     string
	imageSHA      string
}

// New получить инстанс Helper'а
func New(opts *Options) (*Helper, error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return nil, err
	}
	h := &Helper{
		opts:    opts,
		homeDir: homeDir,
	}
	err = h.setupVault()
	if err != nil {
		return nil, err
	}
	err = h.setupDockerRegistry()
	if err != nil {
		return nil, err
	}
	err = h.processDockerImageDigest()
	if err != nil {
		return nil, err
	}
	err = h.setupDockerImageRE()
	if err != nil {
		return nil, err
	}
	return h, h.setupCurrentUser()
}

func (h *Helper) setupVault() error {
	v, err := vault.New(&h.opts.Vault)
	if err != nil {
		return err
	}
	h.vault = v
	return nil
}

func (h *Helper) setupDockerRegistry() error {
	var (
		key string
		err error
	)
	if len(h.opts.DockerAuthConfig) > 0 {
		key, err = registry.DockerRegistryKeyFromConfig(h.opts.DockerAuthConfig, h.opts.DockerImage)
	}
	if len(h.opts.GCloudKey) > 0 {
		key, err = h.vault.ReadByPath(h.opts.GCloudKey)
	}
	h.registry = registry.New(key)
	return err
}

func (h *Helper) processDockerImageDigest() error {
	ref, err := name.ParseReference(h.opts.DockerImage)
	if err != nil {
		return err
	}
	h.imageName = ref.Context().String()
	digest, err := h.registry.Digest(h.opts.DockerImage)
	if err != nil {
		return err
	}
	h.imageSHA = digest
	return nil
}

func (h *Helper) setupDockerImageRE() error {
	re, err := regexp.Compile(
		fmt.Sprintf(`(image:.+%s)[:@](.*)`, h.imageName),
	)
	if err != nil {
		return err
	}
	h.imageDigestRE = re
	return nil
}

// setupCurrentUser настраиваем окружение текущего пользователя, но только в том случае, если каталог $HOME/.ssh/ не
// существует
func (h *Helper) setupCurrentUser() error {
	if !h.isAllowedToModifySSHOptions() {
		return nil
	}
	// Установка пользователя, от имени которого будут производиться обращения к серверу git
	err := git.SetUser(h.opts.GitLab.User.Email, h.opts.GitLab.User.Login)
	if err != nil {
		return err
	}
	return h.modifySSHOptions()
}

// PrepareWorkspace подготовка файлов и создание временных каталогов
func (h *Helper) PrepareWorkspace() error {
	err := h.setupDirs()
	if err != nil {
		return err
	}
	return nil
}

// FetchStates получение репозитория состояний
func (h *Helper) FetchStates() (bool, error) {
	err := git.Clone(h.statesDir, h.opts.StatesRepo)
	if err != nil {
		return false, err
	}
	if git.BranchExists(h.statesDir, h.opts.Branch()) {
		return true, git.Checkout(h.statesDir, h.opts.Branch())
	}
	return false, git.CheckoutNewBranch(h.statesDir, h.opts.Branch())
}

// FetchKustomize получение актуального репозитория с Kustomize спецификациями
func (h *Helper) FetchKustomize() error {
	err := git.Clone(h.kustomizeDir, h.opts.KustomizeRepo)
	if err != nil {
		return err
	}
	return h.RenderKustomizeSpecsFromSource()
}

func (h *Helper) IsStatesContainsServiceSpecs() bool {
	items, err := ioutil.ReadDir(h.resourcesDir)
	if err != nil {
		return true
	}
	for _, item := range items {
		if item.IsDir() {
			continue
		}
		if strings.Contains(item.Name(), h.opts.GitLab.Project.Name) {
			return true
		}
	}
	return false
}

// FetchAll получение актуального состояния рабочих репозиториев
func (h *Helper) FetchAll() error {
	ok, err := h.FetchStates()
	if err != nil {
		return err
	}
	// Если ветка уже существует и в ней есть спецификации сервиса,
	// нет необходимости получать репозиторий с Kustomize спецификациями
	// и производить их рендер
	if ok {
		// Если проверка наличия спецификация отключена специально, то
		// ничего не делаем
		if !h.opts.CheckStatesContainsServiceSpecs {
			return nil
		}
		// Если проверка спецификация производится и спецификации приложения
		// есть в репозитории состояний, то ничего не делаем
		if h.IsStatesContainsServiceSpecs() {
			return nil
		}
	}
	return h.FetchKustomize()
}

// RenderKustomizeSpecsFromSource рендер Kustomize спецификаций в каталог состояния, Kustomize спецификации, как и
// репозиторий состояния должны быть получены на момент выполнения этой команды. На всякий случай, удаляем каталог с
// состоянием, это может пригодиться, если политика рендера будет настраиваемой, например, пользователь через флаг или
// через переменную окружения захочет задать ререндер текущего состояния
func (h *Helper) RenderKustomizeSpecsFromSource() error {
	// В случае статического окружения не удаляем каталог с имеющимся состоянием,
	// так как нам нужно только добавить новых спецификация не затрагивая старые,
	// так же используем каталог, в котором хранятся только спецификации сервиса
	// без каких-либо зависимостей. В случае динамического окружения рендер будет
	// происходить вместе со всеми необходимым для сервиса зависимостями.
	servicePath := path.Join("services", h.opts.GitLab.Project.Name, "resources")
	if h.opts.EnvironmentType != "static" {
		os.RemoveAll(h.resourcesDir)
		err := os.MkdirAll(h.resourcesDir, 0766)
		if err != nil {
			return err
		}
		servicePath = path.Join("services", h.opts.GitLab.Project.Name)
	}

	projectPath := path.Join(h.kustomizeDir, servicePath)

	// Проверяем заранее, чтобы сделать ошибку более понятной для пользователей, без проверки будет
	// возвращен указатель на временный каталог и никакой информации о том, что произошло, пример:
	// lstat /tmp/kustomize810780915/services/pfa-resource-personalization: no such file or directory
	if _, err := os.Stat(projectPath); err != nil {
		return fmt.Errorf("directory %s does not exist in %s repository", servicePath, h.opts.KustomizeRepo)
	}

	bytes, err := kustomize.Build(projectPath)
	if err != nil {
		return err
	}
	return k8ssplit.Split(bytes, h.resourcesDir)
}

// PushChanges фиксация в репозитории состояний
func (h *Helper) PushChanges() error {
	// Отсутствие изменений – не ошибка
	if !git.ChangesExists(h.statesDir) {
		return nil
	}
	message := fmt.Sprintf("[%s] Change image reference to %s", h.opts.GitLab.Project.Name, h.imageSHA)
	return git.CommitAndPush(h.statesDir, h.opts.Branch(), message)
}

// Cleanup удаление временных каталогов и файлов
func (h *Helper) Cleanup() error {
	for _, dir := range []string{
		h.statesDir,
		h.kustomizeDir,
	} {
		err := os.RemoveAll(dir)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *Helper) setupDirs() error {
	statesDir, err := ioutil.TempDir("", "states")
	if err != nil {
		return err
	}
	kustomizeDir, err := ioutil.TempDir("", "kustomize")
	if err != nil {
		return err
	}
	h.statesDir = statesDir
	h.resourcesDir = path.Join(h.statesDir, "resources")
	h.kustomizeDir = kustomizeDir
	return nil
}

// CreateEnvironment создание окружения
func (h *Helper) CreateEnvironment() error {
	if argocd.AppExists(h.opts.Environment()) {
		return nil
	}
	return argocd.AppCreate(h.opts.Environment(), &argocd.AppCreateOptions{
		Repo:       h.opts.StatesRepo,
		Server:     h.opts.ArgoCD.Server,
		Project:    h.opts.ArgoCD.AppProject,
		Namespace:  h.opts.GitLab.Environment.Name,
		Revision:   h.opts.Branch(),
		SyncPolicy: appCreateSyncPolicy,
		SelfHeal:   appCreateSelfHeal,
		AutoPrune:  appCreateAutoPrune,
	})
}

// WaitEnvironment ожидание запуска окружение, таймаут в секундах задается через переменную окружения `ARGOCD_WAIT_TIMEOUT`
// Если задан селектор, то будет происходить ожидание только этого ресурса, аналогично флагу `--resource` у ArgoCD
func (h *Helper) WaitEnvironment(resourceSelector string) bool {
	if len(resourceSelector) == 0 {
		return argocd.AppWait(h.opts.Environment(), h.opts.ArgoCD.WaitTimeout)
	}
	return argocd.AppWaitResource(h.opts.Environment(), resourceSelector, h.opts.ArgoCD.WaitTimeout)
}

func (h *Helper) destroyEnvironment() error {
	if !argocd.AppExists(h.opts.Environment()) {
		return nil
	}
	err := argocd.AppDelete(h.opts.Environment())
	if err != nil {
		return err
	}
	// Клиент отправляет неблокирующий запрос, что, собственно, не означает, что все ресурсы, в том числе приложение,
	// корректно удалены из ArgoCD, поэтому следим некоторое время, что приложение исчезло из листинга приложений
	timeout := time.After(time.Duration(h.opts.ArgoCD.WaitTimeout) * time.Second)
	ticker := time.NewTicker(3 * time.Second)
	for {
		select {
		case <-timeout:
			return errors.New("timeout")
		case <-ticker.C:
			exists := argocd.AppExists(h.opts.Environment())
			if !exists {
				return nil
			}
		}
	}
}

// DestroyEnvironment удаление окружения с его вветкой в репозитории состояний
func (h *Helper) DestroyEnvironment() error {
	if err := h.destroyEnvironment(); err != nil {
		return err
	}
	return h.removeRemoteBranch(h.opts.StatesRepo, h.opts.Branch())
}

// isAllowedToModifySSHOptions проверка возможности модифицировать домашнюю директорию
func (h *Helper) isAllowedToModifySSHOptions() bool {
	_, err := os.Stat(path.Join(h.homeDir, ".ssh"))
	return os.IsNotExist(err)
}

// modifySSHOptions создание каталога `.ssh` в домашнем каталоге, получение Deploy Key из Vault и сохранение ключа в
// созданный ранее каталог, получение SSH публичный ключей по хостам из настроек
func (h *Helper) modifySSHOptions() error {
	d, err := dotssh.New(h.homeDir)
	if err != nil {
		return err
	}
	key, err := h.vault.ReadByPath(h.opts.DeployKey)
	if err != nil {
		return err
	}
	err = d.SetKey(key)
	if err != nil {
		return err
	}
	return d.GatherPublicKey(h.opts.GitLab.ServerHost)
}

// ReplaceImageDigest замена Image Digest в каталоге репозитория состояний
func (h *Helper) ReplaceImageDigest() (int, error) {
	var (
		replacements       int
		imageRefWithPrefix string
	)
	files, err := ioutil.ReadDir(h.resourcesDir)
	if err != nil {
		return 0, nil
	}

	// Ссылка на Docker Image может содержать в себе тег, хотя, фактически, использоваться не будет,
	// если при этом задается SHA-референс, убедиться можно так:
	//
	// % docker pull alpine:3.11
	// Digest: sha256:cb8a924afdf0229ef7515d9e5b3024e23b3eb03ddbba287f4a19c6ac90b8d221
	//
	// % docker pull alpine:3.11@sha256:cb8a924afdf0229ef7515d9e5b3024e23b3eb03ddbba287f4a19c6ac90b8d221
	// Status: Image is up to date for alpine@sha256:cb8a924afdf0229ef7515d9e5b3024e23b3eb03ddbba287f4a19c6ac90b8d221
	//
	// % docker pull alpine:3.12@sha256:cb8a924afdf0229ef7515d9e5b3024e23b3eb03ddbba287f4a19c6ac90b8d221
	// Status: Image is up to date for alpine@sha256:cb8a924afdf0229ef7515d9e5b3024e23b3eb03ddbba287f4a19c6ac90b8d221
	//
	// При этом:
	//
	// % docker pull alpine:3.12
	// Error response from daemon: manifest for alpine:3.12 not found: manifest unknown: manifest unknown
	if len(h.opts.DockerTag) > 0 {
		imageRefWithPrefix = ":" + h.opts.DockerTag + "@" + h.imageSHA
	} else {
		imageRefWithPrefix = "@" + h.imageSHA
	}

	replacement := fmt.Sprintf("$1%s", imageRefWithPrefix)
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		filename := path.Join(h.resourcesDir, file.Name())
		b, err := ioutil.ReadFile(filename)
		if err != nil {
			return 0, err
		}
		if !h.imageDigestRE.Match(b) {
			continue
		}
		replacements++
		b = h.imageDigestRE.ReplaceAll(b, []byte(replacement))
		err = ioutil.WriteFile(filename, b, file.Mode())
		if err != nil {
			return 0, err
		}
	}
	return replacements, nil
}

// removeRemoteBranch удаление remote ветки, для этой операции придется сначала склонировать репозиторий, а только потом
// производить пуш с удалением, либо, создавать пустой репозиторий, сеттить url репозиторий и потом пушить
func (h *Helper) removeRemoteBranch(repo, branch string) error {
	dir, err := ioutil.TempDir("", "remove-repo-branch")
	if err != nil {
		return err
	}
	defer os.RemoveAll(dir)
	err = git.Clone(dir, repo)
	if err != nil {
		return err
	}
	// Удаление несуществующей ветки приводит к ошибке: remote ref does not exist
	if !git.BranchExists(dir, branch) {
		return nil
	}
	return git.DeleteBranch(dir, branch)
}
