package helper

import (
	"testing"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/argocd"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/gitlab"
)

func TestApplicationURL(t *testing.T) {
	tests := []struct {
		o      Options
		result string
	}{
		{
			o: Options{
				ArgoCD: argocd.Options{
					Server: "http://[fe80::%31%25en0]:8080/",
				},
			},
			result: "https:///applications/-",
		},
		{
			o:      Options{},
			result: "https:///applications/-",
		},
		{
			o: Options{
				ArgoCD: argocd.Options{
					Auth: argocd.AuthOptions{
						Server: "localhost",
					},
				},
				GitLab: gitlab.Options{
					Project: gitlab.ProjectOptions{
						Namespace: "PFA",
					},
					Environment: gitlab.EnvironmentOptions{
						Name: "foobar",
					},
				},
			},
			result: "https://localhost/applications/pfa-foobar",
		},
		{
			o: Options{
				ArgoCD: argocd.Options{
					Auth: argocd.AuthOptions{
						Server: "http://localhost:8080",
					},
				},
				GitLab: gitlab.Options{
					Project: gitlab.ProjectOptions{
						Namespace: "PFA",
					},
					Environment: gitlab.EnvironmentOptions{
						Name: "foobar",
					},
				},
			},
			result: "http://localhost:8080/applications/pfa-foobar",
		},
	}
	for id, test := range tests {
		res := test.o.ApplicationURL()
		if res != test.result {
			t.Errorf("%d. Must be %q, but got %q", id, test.result, res)
		}
	}
}

func TestBranch(t *testing.T) {
	tests := []struct {
		o      Options
		result string
	}{
		{
			o: Options{
				GitLab: gitlab.Options{
					Branch: "feature/branch",
					Environment: gitlab.EnvironmentOptions{
						Name: "environment",
					},
				},
			},
			result: "feature/branch",
		},
		{
			o: Options{
				BranchPrefix:    "",
				EnvironmentType: "dynamic",
				GitLab: gitlab.Options{
					Branch: "feature/branch",
					Environment: gitlab.EnvironmentOptions{
						Name: "environment",
					},
				},
			},
			result: "feature/branch",
		},
		{
			o: Options{
				BranchPrefix:    "autotest/",
				EnvironmentType: "dynamic",
				GitLab: gitlab.Options{
					Branch: "feature/branch",
					Environment: gitlab.EnvironmentOptions{
						Name: "environment",
					},
				},
			},
			result: "autotest/feature/branch",
		},
		{
			o: Options{
				BranchPrefix:    "autotest",
				EnvironmentType: "dynamic",
				GitLab: gitlab.Options{
					Branch: "feature/branch",
					Environment: gitlab.EnvironmentOptions{
						Name: "environment",
					},
				},
			},
			result: "autotest/feature/branch",
		},
		{
			o: Options{
				EnvironmentType: "static",
				GitLab: gitlab.Options{
					Branch: "feature/branch",
					Environment: gitlab.EnvironmentOptions{
						Name: "qa1",
					},
				},
			},
			result: "qa1",
		},
	}
	for _, test := range tests {
		res := test.o.Branch()
		if res != test.result {
			t.Errorf("Must be %q, but got %q", test.result, res)
		}
	}
}
