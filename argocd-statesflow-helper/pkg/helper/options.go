package helper

import (
	"fmt"
	"net/url"
	"strings"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/argocd"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/gitlab"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/vault"
)

// Options параметры для работы helper'а
type Options struct {
	// Есть два способа получения доступа к Docker Registry – Service Account и Docker Config, в котором, конечно, может
	// находится все тот же Service Account. Сначала проверяется `DOCKER_AUTH_CONFIG`, так как может содержать описание
	// доступа не только к Google Docker Registry, затем проверяется `STATESFLOW_GCLOUD_KEY`
	DockerAuthConfig string `envconfig:"DOCKER_AUTH_CONFIG" desc:"Конфигурация Docker в JSON для доступа к Registry" source:"GitLab Variables (уровень группы проектов)"`
	GCloudKey        string `envconfig:"STATESFLOW_GCLOUD_KEY" desc:"Путь в формате kv2 к Service Account для доступа к Google Cloud Registry в Vault" source:"GitLab Variables (уровень группы проектов)"`

	// CheckStatesContainsServiceSpecs актуален только для обновляемых окружений, которые уже были созданы ранее и будет
	// удобен при заведении нового окружения на стенд через обычную его публикацию
	CheckStatesContainsServiceSpecs bool `envconfig:"STATESFLOW_CHECK_STATES_CONTAINS_SERVICE_SPECS" default:"true" desc:"Проверять наличие спецификаций в states репозитории" source:"GitLab Variables (уровень проекта)"`
	// EnvironmentType бывает двух типов – dynamic и static, в первом случае для репозитория состояний будет использована
	// текущая ветка в репозитории приложения (GitLab.Branch), во втором случае в качестве ветки назначения будет использоваться
	// имя окружения (GitLab.Environment.Name)
	EnvironmentType string `envconfig:"STATESFLOW_ENVIRONMENT_TYPE" required:"true" desc:"Тип создаваемого окружения (static, dynamic)" source:"Файл .gitlab-ci.yml (параметр variables)"`
	// BranchPrefix задается при вызове команд создания и удаления окружения с помощью флага `--branch-prefix` и используется
	// при формировании ветки в репозитории состояний
	BranchPrefix string

	DeployKey     string `envconfig:"STATESFLOW_DEPLOY_KEY" required:"true" desc:"Путь в формате kv2 к Deploy Key в Vault, используемого для доступа к репозиториям" source:"GitLab Variables (уровень группы проектов)"`
	KustomizeRepo string `envconfig:"STATESFLOW_KUSTOMIZE_REPO" required:"true" desc:"Репозиторий, в котором хранятся спецификации Kustomize" source:"GitLab Variables (уровень группы проектов)"`
	StatesRepo    string `envconfig:"STATESFLOW_STATES_REPO" required:"true" desc:"Репозиторий, в котором хранятся состояния, репозиторий должен быть подключен к ArgoCD" source:"GitLab Variables (уровень группы проектов)"`
	DockerImage   string `envconfig:"STATESFLOW_DOCKER_IMAGE" required:"true" desc:"Docker-образ для публикации" source:"Файл .gitlab-ci.yml (параметр variables)"`
	DockerTag     string `envconfig:"STATESFLOW_DOCKER_TAG" desc:"Использовать заданный тег вместе с SHA-референсом" source:"Файл .gitlab-ci.yml (параметр variables)"`
	ArgoCD        argocd.Options
	GitLab        gitlab.Options
	Vault         vault.Options
}

// Environment имя окружения с Project Namespace префиксом
func (o *Options) Environment() string {
	return fmt.Sprintf(
		"%s-%s",
		Sanitize(o.GitLab.Project.Namespace),
		o.GitLab.Environment.Name,
	)
}

// Branch имя ветки относительно типа окружения и заданного префикса
func (o *Options) Branch() string {
	if o.EnvironmentType == "static" {
		return o.GitLab.Environment.Name
	}
	if len(o.BranchPrefix) == 0 {
		return o.GitLab.Branch
	}
	prefix := strings.TrimSuffix(o.BranchPrefix, "/")
	return fmt.Sprintf("%s/%s", prefix, o.GitLab.Branch)
}

// ApplicationURL ссылка на приложение в ArgoCD
func (o *Options) ApplicationURL() string {
	u, err := url.Parse(o.ArgoCD.Auth.Server)
	if err != nil {
		// :ops:
		return ""
	}
	if len(u.Scheme) == 0 {
		u.Scheme = "https"
	}
	if len(u.Host) == 0 {
		u.Host = u.Path
	}
	u.Path = fmt.Sprintf("/applications/%s", o.Environment())
	return u.String()
}
