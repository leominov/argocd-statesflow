package helper

import (
	"regexp"
	"strings"
)

var (
	regexpNonAuthorizedChars = regexp.MustCompile("[^a-zA-Z0-9-_]")
	regexpMultipleDashes     = regexp.MustCompile("-+")
)

// Sanitize удалить из строки все спецсимволы, используется для формирования имени окружения
func Sanitize(name string) string {
	slug := strings.TrimSpace(name)
	slug = strings.ToLower(slug)
	slug = regexpNonAuthorizedChars.ReplaceAllString(slug, "-")
	slug = regexpMultipleDashes.ReplaceAllString(slug, "-")
	slug = strings.Trim(slug, "-_")
	return slug
}
