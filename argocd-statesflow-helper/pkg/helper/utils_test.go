package helper

import "testing"

func TestSanitize(t *testing.T) {
	tests := map[string]string{
		"pfa!/golang":      "pfa-golang",
		" pfa/golang/libs": "pfa-golang-libs",
		"PFA":              "pfa",
	}
	for in, out := range tests {
		res := Sanitize(in)
		if out != res {
			t.Errorf("Must be %q, but got %q", out, res)
		}
	}
}
