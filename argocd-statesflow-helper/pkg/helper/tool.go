package helper

import (
	"os"
	"os/exec"
	"strings"
)

type Tool struct {
	Path   string
	Reason string
}

// TODO(l.aminov): Двинуть в отдельный пакет
func (t *Tool) Exists() bool {
	path := os.ExpandEnv(t.Path)
	if strings.HasPrefix(path, "/") {
		_, err := os.Stat(path)
		return err == nil
	}
	_, err := exec.LookPath(path)
	return err == nil
}
