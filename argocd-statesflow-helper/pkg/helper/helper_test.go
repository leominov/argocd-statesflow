package helper

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/gitlab"
)

func TestHelper_ReplaceImageDigestWithoutTag(t *testing.T) {
	h := &Helper{
		opts:      &Options{},
		imageSHA:  "sha256:ABCD",
		imageName: "auth",
	}
	err := h.setupDockerImageRE()
	if err != nil {
		t.Fatal(err)
	}
	err = h.PrepareWorkspace()
	if err != nil {
		t.Fatal(err)
	}
	b, err := ioutil.ReadFile("test_data/pfa-auth.Deployment.yaml")
	if err != nil {
		t.Fatal(err)
	}
	err = os.MkdirAll(h.resourcesDir, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	err = ioutil.WriteFile(path.Join(h.resourcesDir, "pfa-auth.Deployment.yaml"), b, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	n, err := h.ReplaceImageDigest()
	assert.NoError(t, err)
	assert.Equal(t, 1, n)
	b, err = ioutil.ReadFile(path.Join(h.resourcesDir, "pfa-auth.Deployment.yaml"))
	if err != nil {
		t.Fatal(err)
	}
	assert.Contains(t, string(b), "auth@sha256:ABCD")
}

func TestHelper_ReplaceImageDigestWithTag(t *testing.T) {
	h := &Helper{
		opts: &Options{
			DockerTag: "1.0.0",
		},
		imageSHA:  "sha256:ABCD",
		imageName: "auth",
	}
	err := h.setupDockerImageRE()
	if err != nil {
		t.Fatal(err)
	}
	err = h.PrepareWorkspace()
	if err != nil {
		t.Fatal(err)
	}
	b, err := ioutil.ReadFile("test_data/pfa-auth.Deployment.yaml")
	if err != nil {
		t.Fatal(err)
	}
	err = os.MkdirAll(h.resourcesDir, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	err = ioutil.WriteFile(path.Join(h.resourcesDir, "pfa-auth.Deployment.yaml"), b, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	n, err := h.ReplaceImageDigest()
	assert.NoError(t, err)
	assert.Equal(t, 1, n)
	b, err = ioutil.ReadFile(path.Join(h.resourcesDir, "pfa-auth.Deployment.yaml"))
	if err != nil {
		t.Fatal(err)
	}
	assert.Contains(t, string(b), "auth:1.0.0@sha256:ABCD")
}

func TestHelper_IsStatesContainsServiceSpecs(t *testing.T) {
	opts := &Options{}
	opts.GitLab.Project = gitlab.ProjectOptions{
		Name:      "foobar",
		Namespace: "group/foonar",
	}
	h := &Helper{
		opts:      opts,
		imageSHA:  "sha256:ABCD",
		imageName: "auth",
	}
	err := h.PrepareWorkspace()
	if err != nil {
		t.Fatal(err)
	}
	err = os.MkdirAll(h.resourcesDir, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}

	assert.False(t, h.IsStatesContainsServiceSpecs())

	err = ioutil.WriteFile(path.Join(h.resourcesDir, "blabla.Deployment.yaml"), []byte(``), os.ModePerm)
	assert.NoError(t, err)

	assert.False(t, h.IsStatesContainsServiceSpecs())

	err = ioutil.WriteFile(path.Join(h.resourcesDir, "foobar.Deployment.yaml"), []byte(``), os.ModePerm)
	assert.NoError(t, err)

	assert.True(t, h.IsStatesContainsServiceSpecs())
}
