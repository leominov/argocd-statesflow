package git

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func TestClone(t *testing.T) {
	dir, err := ioutil.TempDir("", "git-clone")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	err = Clone(dir, "https://gitlab-rnd.tcsbank.ru/devops-public/vault-secret.git")
	if err != nil {
		t.Error(err)
	}
	err = Clone(dir, "https://gitlab-rnd.tcsbank.ru/devops-public/vault-secret.git")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = os.Stat(path.Join(dir, "README.md"))
	if err != nil {
		t.Error(err)
	}
	if !BranchExists(dir, "master") {
		t.Error("Must be true, but got false")
	}
	if BranchExists(dir, "git-clone-not-found-branch") {
		t.Error("Must be false, but got true")
	}
	if ChangesExists(dir) {
		t.Error("Must be false, but got true")
	}
	err = ioutil.WriteFile(path.Join(dir, "test-file"), []byte{}, os.ModePerm)
	if err != nil {
		t.Error(err)
	}
	if !ChangesExists(dir) {
		t.Error("Must be true, but got false")
	}
}

func TestCheckoutNewBranch(t *testing.T) {
	dir, err := ioutil.TempDir("", "checkout-new-branch")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	err = Clone(dir, "https://gitlab-rnd.tcsbank.ru/devops-public/vault-secret.git")
	if err != nil {
		t.Error(err)
	}
	if err := CheckoutNewBranch(dir, "feature/1"); err != nil {
		t.Error(err)
	}
	if err := CheckoutNewBranch(dir, "feature/1"); err == nil {
		t.Error("Must be an error, but got nil")
	}
}

func TestCheckout(t *testing.T) {
	dir, err := ioutil.TempDir("", "checkout-new-branch")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	err = Clone(dir, "https://gitlab-rnd.tcsbank.ru/devops-public/vault-secret.git")
	if err != nil {
		t.Error(err)
	}
	if err := CheckoutNewBranch(dir, "feature/1"); err != nil {
		t.Error(err)
	}
	if err := Checkout(dir, "master"); err != nil {
		t.Error(err)
	}
	// Same result on second run
	if err := Checkout(dir, "master"); err != nil {
		t.Error(err)
	}
	if err := Checkout(dir, "foobar"); err == nil {
		t.Error("Must be an error, but got nil")
	}
}
