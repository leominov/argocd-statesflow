package git

import (
	"fmt"
	"os/exec"
)

// Clone клонировать репозиторий
func Clone(dir, repo string) error {
	b, err := gitCommand(dir, "clone", repo, ".").CombinedOutput()
	if err != nil {
		return fmt.Errorf("git repo clone error: %v. output: %s", err, string(b))
	}
	return nil
}

// Checkout переключиться на commitLike
func Checkout(dir, commitLike string) error {
	co := gitCommand(dir, "checkout", commitLike)
	if b, err := co.CombinedOutput(); err != nil {
		return fmt.Errorf("error checking out %s: %v. output: %s", commitLike, err, string(b))
	}
	return nil
}

// CheckoutNewBranch переключиться на новую ветку
func CheckoutNewBranch(dir, branch string) error {
	co := gitCommand(dir, "checkout", "-b", branch)
	if b, err := co.CombinedOutput(); err != nil {
		return fmt.Errorf("error checking out %s: %v. output: %s", branch, err, string(b))
	}
	return nil
}

// BranchExists проверить наличие ветки
func BranchExists(dir, branch string) bool {
	heads := "origin"
	// $ git ls-remote --heads origin feature/PFA-0001
	// b90b5bcb15dedeee54d9a2dcceb5d14489240176	refs/heads/autotest-feature-0ac0em/feature/PFA-0001
	// $ git ls-remote --heads origin autotest-feature-0ac0em/feature/PFA-0001
	// b90b5bcb15dedeee54d9a2dcceb5d14489240176	refs/heads/autotest-feature-0ac0em/feature/PFA-0001
	ref := fmt.Sprintf("refs/heads/%s", branch)
	co := gitCommand(dir, "ls-remote", "--exit-code", "--heads", heads, ref)
	return co.Run() == nil
}

// DeleteBranch удаление ветки
// ref: https://www.educative.io/edpresso/how-to-delete-remote-branches-in-git
func DeleteBranch(dir, branch string) error {
	b, err := gitCommand(dir, "push", "origin", "--delete", branch).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error deleting %s branch: %v. output: %s", branch, err, string(b))
	}
	return nil
}

// ChangesExists проверить наличие изменений в каталоге
func ChangesExists(dir string) bool {
	b, _ := gitCommand(dir, "status", "--short").CombinedOutput()
	return len(b) != 0
}

// CommitAndPush добавить измененые файлы, зафиксировать изменения и отправить серверу git
func CommitAndPush(dir, branch, message string) error {
	b, err := gitCommand(dir, "add", "./").CombinedOutput()
	if err != nil {
		return fmt.Errorf("error adding files: %v. output: %s", err, string(b))
	}
	b, err = gitCommand(dir, "commit", "-am", message).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error committing: %v. output: %s", err, string(b))
	}
	b, err = gitCommand(dir, "push", "--set-upstream", "origin", branch).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error pushing changes: %v. output: %s", err, string(b))
	}
	return nil
}

// SetUser установить настройки пользователя для клиента git
func SetUser(email, name string) error {
	b, err := gitCommand("", "config", "--global", "user.email", email).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error setting user.email: %v. output: %s", err, string(b))
	}
	b, err = gitCommand("", "config", "--global", "user.name", name).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error setting user.name: %v. output: %s", err, string(b))
	}
	return nil
}

func gitCommand(dir string, arg ...string) *exec.Cmd {
	cmd := exec.Command("git", arg...)
	if len(dir) > 0 {
		cmd.Dir = dir
	}
	return cmd
}
