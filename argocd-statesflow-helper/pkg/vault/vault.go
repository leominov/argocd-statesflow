package vault

import (
	"fmt"
	"io"
	"strings"

	"github.com/hashicorp/vault/api"
)

// Options содержат пользовательские настройки подключения к Vault, набор соответствует
// привычному набору параметров за исключением версии KV, так как хранилища такого типа
// мы уже не применяем и не создаем. Поддерживаемые методы:
// * Username & Password (https://www.vaultproject.io/api-docs/auth/userpass/)
// * LDAP (https://www.vaultproject.io/api-docs/auth/ldap/)
//
// В последующем настройки должны будут переделаны на работу с AppRole и Token.
// ref(1): https://www.vaultproject.io/docs/auth/approle/
// ref(2): https://www.vaultproject.io/docs/auth/token/
type Options struct {
	Address    string `envconfig:"VAULT_ADDR" default:"vault-rnd.tcsbank.ru" required:"true" desc:"Сервера Vault" source:"GitLab Variables (уровень группы проектов)"`
	AuthMethod string `envconfig:"VAULT_METHOD" default:"ldap" required:"true" desc:"Метод аутентификации Vault (ldap или userpass)" source:"GitLab Variables (уровень группы проектов)"`
	Login      string `envconfig:"VAULT_LOGIN" required:"true" desc:"Логин для обращения к Vault" source:"GitLab Variables (уровень группы проектов)"`
	Password   string `envconfig:"VAULT_PASSWORD" required:"true" desc:"Пароль для обращения к Vault" source:"GitLab Variables (уровень группы проектов)"`
}

// Vault реализует враппер для оригинального клиент Vault, для более простого доступа к
// секретам
type Vault struct {
	client *api.Client
}

// ParseSecretPath получить путь, поле и параметры по адресу секрета
func ParseSecretPath(secretPath string) (path, field string, params map[string]string) {
	parts := strings.Split(strings.TrimPrefix(secretPath, "vault:"), "#")
	if len(parts) < 2 {
		return
	}
	path = parts[0]
	field = parts[1]
	if len(parts) == 3 {
		params = map[string]string{
			"version": parts[2],
		}
	}
	return
}

// New создать новый клиент Vault
func New(o *Options) (*Vault, error) {
	config := api.Config{
		Address: o.Address,
	}
	client, err := api.NewClient(&config)
	if err != nil {
		return nil, err
	}
	options := map[string]interface{}{
		"password": o.Password,
	}
	path := fmt.Sprintf("auth/%s/login/%s", o.AuthMethod, o.Login)
	secret, err := client.Logical().Write(path, options)
	if err != nil {
		return nil, err
	}
	client.SetToken(secret.Auth.ClientToken)
	return &Vault{client}, nil
}

// ReadByPath чтение значения, передаваемого в формате: vault:path/to/secret#field#version
func (v *Vault) ReadByPath(secretPath string) (string, error) {
	path, field, params := ParseSecretPath(secretPath)
	if len(path) == 0 {
		return "", fmt.Errorf("unknown secret path format: %s", secretPath)
	}
	secret, err := v.ReadSecret(path, params)
	if err != nil {
		return "", err
	}
	return GetSecretField(secret, field), nil
}

// ReadSecret прочитать секрет из Vault
func (v *Vault) ReadSecret(path string, params map[string]string) (*api.Secret, error) {
	r := v.client.NewRequest("GET", "/v1/"+path)
	for k, v := range params {
		r.Params.Set(k, v)
	}
	resp, err := v.client.RawRequest(r)
	if resp != nil {
		defer resp.Body.Close()
	}
	if resp != nil && resp.StatusCode == 404 {
		secret, parseErr := api.ParseSecret(resp.Body)
		switch parseErr {
		case nil:
		case io.EOF:
			return nil, nil
		default:
			return nil, err
		}
		if secret != nil && (len(secret.Warnings) > 0 || len(secret.Data) > 0) {
			return secret, nil
		}
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return api.ParseSecret(resp.Body)
}

// GetSecretField прочитать из секрета указанное поле, если поле не найдено, будет возвращена пустая строка
func GetSecretField(secret *api.Secret, field string) string {
	data, ok := secret.Data["data"]
	if !ok || data == nil {
		return ""
	}
	val := data.(map[string]interface{})[field]
	if val == nil {
		return ""
	}
	return val.(string)
}
