package vault

import (
	"reflect"
	"testing"
)

func TestParseSecretPath(t *testing.T) {
	tests := []struct {
		in     string
		path   string
		field  string
		params map[string]string
	}{
		{
			in:    "advert/data/path/to/secret#field#version",
			path:  "advert/data/path/to/secret",
			field: "field",
			params: map[string]string{
				"version": "version",
			},
		},
		{
			in:     "vault:advert/data/path/to/secret#field",
			path:   "advert/data/path/to/secret",
			field:  "field",
			params: nil,
		},
		{
			in:     "vault:advert/data/path/to/secret",
			path:   "",
			field:  "",
			params: nil,
		},
	}
	for _, test := range tests {
		path, field, params := ParseSecretPath(test.in)
		if test.path != path {
			t.Errorf("Path must be %q, but got %q", test.path, path)
		}
		if test.field != field {
			t.Errorf("Field must be %q, but got %q", test.field, field)
		}
		if !reflect.DeepEqual(test.params, params) {
			t.Errorf("Params must be %v, but got %v", test.params, params)
		}
	}
}
