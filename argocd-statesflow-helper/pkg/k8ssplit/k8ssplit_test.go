package k8ssplit

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestSplit(t *testing.T) {
	dir, err := ioutil.TempDir("", "k8ssplit")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	b, err := ioutil.ReadFile("test_data/invalid.yaml")
	if err != nil {
		t.Fatal(err)
	}
	err = Split(b, dir)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	b, err = ioutil.ReadFile("test_data/valid.yaml")
	if err != nil {
		t.Fatal(err)
	}
	err = Split(b, dir)
	if err != nil {
		t.Error(err)
	}
}
