package k8ssplit

import (
	"bytes"
	"fmt"
	"os/exec"
)

// Split разбиение мультидокумента и сохранение в каталог
func Split(doc []byte, dest string) error {
	buffer := bytes.Buffer{}
	buffer.Write(doc)
	co := exec.Command("k8s-split", "-q", "-f", "-", "-o", dest)
	co.Stdin = &buffer
	b, err := co.CombinedOutput()
	if err != nil {
		return fmt.Errorf("error k8s-split: %v. output: %s", err, string(b))
	}
	return nil
}
