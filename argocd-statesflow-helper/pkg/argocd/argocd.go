package argocd

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/execcmd"
)

// Check проверка доступности сервера ArgoCD, а вместе с тем и проверка
// совместимости версий
func Check() error {
	b, err := execcmd.WithRetry("", "argocd", "version")
	if err != nil {
		return fmt.Errorf("argocd version error: %v. output: %s", err, string(b))
	}
	return nil
}

// AppWait ожидание запуска ресурса приложения
func AppWaitResource(appName, resourceSelector string, timeout int) bool {
	return argoCommand(
		"app",
		"wait",
		appName,
		fmt.Sprintf("--timeout=%d", timeout),
		fmt.Sprintf("--resource=%s", resourceSelector),
	).Run() == nil
}

// AppWait ожидание запуска приложения
func AppWait(appName string, timeout int) bool {
	return argoCommand(
		"app",
		"wait",
		appName,
		fmt.Sprintf("--timeout=%d", timeout),
	).Run() == nil
}

// AppCreateOptions настройки команды создания приложения в ArgoCD. Через переменные окружения можно передавать только
// настройки подключения, например, адрес сервера и токен, все остальное только через флаги
type AppCreateOptions struct {
	Repo      string
	Server    string
	Project   string
	Namespace string
	Revision  string
	// Sync options
	SyncPolicy string // automated or none
	SelfHeal   bool   // when sync is automated
	AutoPrune  bool   // when sync is automated
}

// AppCreate создание приложения в ArgoCD
func AppCreate(appName string, opts *AppCreateOptions) error {
	args := []string{
		"app",
		"create",
		appName,
		"--path=resources",
		// Передача аргументов гарантирует применение заданных параметров
		fmt.Sprintf("--sync-policy=%s", opts.SyncPolicy),
		fmt.Sprintf("--dest-namespace=%s", opts.Namespace),
		fmt.Sprintf("--project=%s", opts.Project),
		fmt.Sprintf("--repo=%s", opts.Repo),
		fmt.Sprintf("--revision=%s", opts.Revision),
		fmt.Sprintf("--dest-server=%s", opts.Server),
	}
	if opts.SyncPolicy == "automated" {
		args = append(args, []string{
			fmt.Sprintf("--self-heal=%s", strconv.FormatBool(opts.SelfHeal)),
			fmt.Sprintf("--auto-prune=%s", strconv.FormatBool(opts.AutoPrune)),
		}...)
	}
	if b, err := argoCommand(args...).CombinedOutput(); err != nil {
		return fmt.Errorf("error creating app %s: %v. output: %s", appName, err, string(b))
	}
	return nil
}

// AppExists проверяет существование приложения в ArgoCD
func AppExists(appName string) bool {
	return argoCommand("app", "get", appName).Run() == nil
}

// AppDelete удаление приложения из ArgoCD
func AppDelete(appName string) error {
	co := argoCommand("app", "delete", appName, "--cascade")
	if b, err := co.CombinedOutput(); err != nil {
		return fmt.Errorf("error deleting %s: %v. output: %s", appName, err, string(b))
	}
	return nil
}

// RepoExists проверка наличия указанного репозитория в списке репозиториев
// подключенных к ArgoCD
func RepoExists(repo string) (bool, error) {
	if len(repo) == 0 {
		return false, fmt.Errorf("empty repo address")
	}
	co := argoCommand("repo", "list")
	b, err := co.CombinedOutput()
	if err != nil {
		return false, fmt.Errorf("error listing repos: %v. output: %s", err, string(b))
	}
	return strings.Contains(string(b), repo), nil
}

func argoCommand(arg ...string) *exec.Cmd {
	cmd := exec.Command("argocd", arg...)
	cmd.Env = os.Environ()
	return cmd
}
