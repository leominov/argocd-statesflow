package argocd

// Options специфические параметры используемые хелпером, а так же параметры подключения к ArgoCD
type Options struct {
	// WaitTimeout по умолчанию выставлен в 10 минут, так как практика показала, что большинство,
	// даже небольших окружений, не всегда успевают за интервал в 5 минут, а искать подходящий
	// таймаут для каждого сервиса никому не хочется.
	WaitTimeout int    `envconfig:"ARGOCD_WAIT_TIMEOUT" required:"true" default:"600" desc:"Время ожидания запуска или удаления окружения" source:"GitLab Variables (уровень группы проектов)"`
	AppProject  string `envconfig:"ARGOCD_APPPROJECT" required:"true" desc:"AppProject для создания окружения в ArgoCD (например, pfa-testing)" source:"GitLab Variables (уровень группы проектов)"`
	Server      string `envconfig:"ARGOCD_DEST_SERVER" required:"true" desc:"Имя Kubernetes-кластера для запуска окружения" source:"GitLab Variables (уровень группы проектов)"`
	Auth        AuthOptions
}

// AuthOptions консольному клиенту ArgoCD можно передать только аргументы подключения к серверу, остальные параметры
// задаются флагами
type AuthOptions struct {
	Server  string `envconfig:"ARGOCD_SERVER" required:"true" default:"argo.gke.tcsbank.ru" desc:"Сервер ArgoCD" source:"GitLab Variables (уровень группы проектов)"`
	Options string `envconfig:"ARGOCD_OPTS" required:"true" default:"--grpc-web --insecure" desc:"Опции для клиента ArgoCD" source:"GitLab Variables (уровень группы проектов)"`
	Token   string `envconfig:"ARGOCD_AUTH_TOKEN" required:"true" desc:"Токен для обращения к ArgoCD" source:"GitLab Variables (уровень группы проектов)"`
}
