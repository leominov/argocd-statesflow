package gitlab

// Options параметры относящиеся к GitLab и GitLab CI
type Options struct {
	ServerHost  string `envconfig:"CI_SERVER_HOST" default:"gitlab-rnd.tcsbank.ru" required:"true" desc:"Хост GitLab" source:"GitLab CI"`
	Branch      string `envconfig:"CI_COMMIT_REF_NAME" required:"true" desc:"Текущия ветка в репозитории приложения" source:"GitLab CI"`
	User        UserOptions
	Project     ProjectOptions
	Environment EnvironmentOptions
}

// UserOptions параметры текущего пользователя GitLab
type UserOptions struct {
	Email string `envconfig:"GITLAB_USER_EMAIL" required:"true" desc:"Для работы с сервером git" source:"GitLab CI"`
	Login string `envconfig:"GITLAB_USER_LOGIN" required:"true" desc:"Для работы с сервером git" source:"GitLab CI"`
}

// ProjectOptions параметры проекта
type ProjectOptions struct {
	Name      string `envconfig:"CI_PROJECT_NAME" required:"true" desc:"Используется в качестве имени сервиса и каталога с Kustomize спецификациями" source:"GitLab CI"`
	Namespace string `envconfig:"CI_PROJECT_NAMESPACE" required:"true" desc:"Пространство имен, будет использоваться в качестве префикса для создаваемого окружения" source:"GitLab CI"`
}

// EnvironmentOptions параметры окружения (задается через gitlab-ci-файл)
type EnvironmentOptions struct {
	Name string `envconfig:"CI_ENVIRONMENT_SLUG" required:"true" desc:"Имя для создаваемого окружения, формируется на основе CI_ENVIRONMENT_NAME" source:"Файл .gitlab-ci.yml (параметр environment.name)"`
	URL  string `envconfig:"CI_ENVIRONMENT_URL" required:"true" desc:"Ссылка на создаваемое окружение" source:"Файл .gitlab-ci.yml (параметр environment.name)"`
}
