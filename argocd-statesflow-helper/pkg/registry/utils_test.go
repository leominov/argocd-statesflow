package registry

import "testing"

func TestDockerRegistryKeyFromConfig(t *testing.T) {
	tests := []struct {
		config, image, result string
		wantErr               bool
	}{
		{
			config:  "",
			image:   "",
			result:  "",
			wantErr: true,
		},
		{
			config:  `{"auths": {}}`,
			image:   "[fe80::%31%25en0]:8080/",
			result:  "",
			wantErr: true,
		},
		{
			config:  `{"auths": {"foobar": {"auth": "Zm9vYmFy"}}}`,
			image:   "localhost",
			result:  "",
			wantErr: true,
		},
		{
			config:  `{"auths": {"localhost": {"auth": "foobar"}}}`,
			image:   "localhost",
			result:  "",
			wantErr: true,
		},
		{
			config:  `{"auths": {"gcr.io": {"auth": "Zm9vYmFy"}}}`,
			image:   "gcr.io/kaniko-project/executor:debug",
			result:  "foobar",
			wantErr: false,
		},
	}
	for _, test := range tests {
		key, err := DockerRegistryKeyFromConfig(test.image, test.config)
		if test.wantErr {
			if err == nil {
				t.Error("Must be an error, but got nil")
			}
			continue
		}
		if err != nil {
			t.Error(err)
		}
		if key != test.result {
			t.Errorf("Must be %s, but got %s", test.result, key)
		}
	}
}
