package registry

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
)

// DockerConfig структура конфигурационного файла
type DockerConfig struct {
	Auths map[string]struct {
		Auth string `json:"auth"`
	} `json:"auths"`
}

// DockerRegistryKeyFromConfig получение авторизационного ключа по конфигурации и имени образа
func DockerRegistryKeyFromConfig(addr, authConfig string) (string, error) {
	dockerConfig := &DockerConfig{}
	err := json.Unmarshal([]byte(authConfig), dockerConfig)
	if err != nil {
		return "", err
	}
	// $ docker pull https://registry.tcsbank.ru:5050/advert-devops/gitlab-dredd
	// invalid reference format
	u, err := url.Parse("https://" + addr)
	if err != nil {
		return "", err
	}
	hostAuth, ok := dockerConfig.Auths[u.Host]
	if !ok {
		return "", fmt.Errorf("auth for %s hostname not found", u.Host)
	}
	b, err := base64.StdEncoding.DecodeString(hostAuth.Auth)
	if err != nil {
		return "", err
	}
	// Внутри строка в формате login:password, пока берем только password, так как знаем, что испольуется только
	// Google Docker Registry, но нужно будет сделать нормальный парсинг
	key := strings.TrimPrefix(string(b), "_json_key:")
	return key, nil
}
