package registry

import (
	"fmt"

	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/google"
	"github.com/google/go-containerregistry/pkg/v1/remote"
)

type Registry struct {
	auth authn.Authenticator
}

// New создание нового клиента Docker Registry на основе ключа в формате JSON
func New(key string) *Registry {
	return &Registry{
		auth: google.NewJSONKeyAuthenticator(key),
	}
}

// Digest возвращает sha256 hash по референсу образа
func (r *Registry) Digest(ref string) (string, error) {
	desc, err := r.getManifest(ref)
	if err != nil {
		return "", fmt.Errorf("get manifest: %v", err)
	}
	return desc.Digest.String(), nil
}

func (r *Registry) getManifest(reference string) (*remote.Descriptor, error) {
	ref, err := name.ParseReference(reference)
	if err != nil {
		return nil, fmt.Errorf("parsing reference %q: %v", reference, err)
	}
	return remote.Get(ref, remote.WithAuth(r.auth))
}
