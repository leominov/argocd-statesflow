package dotssh

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func TestSetKey(t *testing.T) {
	_, err := New("not/found/directrory")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	d := &Dotssh{"not/found/directrory"}
	err = d.SetKey("AABBCCDD")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}

	dir, err := ioutil.TempDir("", "dotssh-set-key")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	d, err = New(dir)
	if err != nil {
		t.Fatal(err)
	}
	err = d.SetKey("AABBCCDD")
	if err != nil {
		t.Fatal(err)
	}
	idrsa := path.Join(dir, ".ssh", "id_rsa")
	f, err := os.Stat(idrsa)
	if err != nil {
		t.Fatal(err)
	}
	if f.Mode() != 0600 {
		t.Errorf("Must be 0600, but got %v", f.Mode())
	}
	err = d.SetKey("AABBCCDD")
	if err != nil {
		t.Error(err)
	}
}

func TestGatherPublicKey(t *testing.T) {
	dir, err := ioutil.TempDir("", "dotssh-gather-key")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	d, err := New(dir)
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		host    string
		wantErr bool
	}{
		{
			host:    "gitlab-rnd.tcsbank.ru",
			wantErr: false,
		},
		{
			// another one
			host:    "gitlab-rnd.tcsbank.ru",
			wantErr: false,
		},
		{
			host:    "not-found-advert-host.com",
			wantErr: true,
		},
		{
			host:    "1",
			wantErr: true,
		},
		{
			host:    "-foobar",
			wantErr: true,
		},
	}
	for _, test := range tests {
		err = d.GatherPublicKey(test.host)
		if test.wantErr {
			if err != nil {
				continue
			}
			t.Errorf("Must be an error for %s, but got nil", test.host)
			continue
		}
		if err != nil {
			t.Error(err)
		}
	}
}
