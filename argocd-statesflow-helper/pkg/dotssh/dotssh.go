package dotssh

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"
)

type Dotssh struct {
	dir string
}

// New получить инстанс Dotssh
func New(homeDir string) (*Dotssh, error) {
	dir := path.Join(homeDir, ".ssh")
	if err := os.Mkdir(dir, 0700); err != nil {
		return nil, err
	}
	return &Dotssh{dir}, nil
}

// SetKey сохранить приватный SSH-ключ
func (d *Dotssh) SetKey(key string) error {
	// Load key "/root/.ssh/id_rsa": invalid format
	key = strings.TrimSpace(key) + "\n"
	idRSA := path.Join(d.dir, "id_rsa")
	if _, err := os.Stat(idRSA); os.IsNotExist(err) {
		return ioutil.WriteFile(idRSA, []byte(key), 0600)
	}
	return nil
}

// GatherPublicKey получить и сохранить публинчый SSH-ключ хоста
func (d *Dotssh) GatherPublicKey(host string) error {
	co := exec.Command("ssh-keyscan", "-T", "10", "-t", "rsa", host)
	// ref: https://bugs.launchpad.net/ubuntu/+source/openssh/+bug/1661745
	co.Env = []string{"RC=1"}
	b, err := co.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to keyscan %s host: %s. output: %s", host, err, string(b))
	}
	// ssh-keyscan возвращает ошибки с нулевым кодом, поэтому проверяем, что на выходе действительно ключ в формате RSA,
	// который мы запрашивали, если ключ не был получен, приложение так же вернет нулевой код
	if !bytes.Contains(b, []byte("ssh-rsa")) {
		return fmt.Errorf("failed to keyscan %s host. output: %s", host, string(b))
	}
	knownHosts := path.Join(d.dir, "known_hosts")
	if _, err = os.Stat(knownHosts); os.IsNotExist(err) {
		return ioutil.WriteFile(knownHosts, b, 0700)
	}
	return nil
}
