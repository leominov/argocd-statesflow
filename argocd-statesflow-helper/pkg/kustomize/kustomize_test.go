package kustomize

import "testing"

func TestBuild(t *testing.T) {
	_, err := Build("test_data/valid")
	if err != nil {
		t.Error(err)
	}
	_, err = Build("test_data/invalid")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}
