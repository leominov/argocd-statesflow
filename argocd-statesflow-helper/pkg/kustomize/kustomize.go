package kustomize

import (
	"fmt"
	"os"
	"os/exec"
)

var (
	kustomizeBuildFlags = []string{
		// Enable plugins, an alpha feature.
		// See https://github.com/kubernetes-sigs/kustomize/blob/master/docs/plugins/README.md
		"--enable_alpha_plugins",
		// If set to 'LoadRestrictionsNone', local kustomizations may load files from outside their root.
		// This does, however, break the relocatability of the kustomization.
		"--load_restrictor=LoadRestrictionsRootOnly",
	}
)

// Build выполняет `kustomize build` для указанной директории, в результируещем документе будет произведена замена
// переменных окружения. Для корректной работы требуется kustomize 3, так как в аргументах, по умолчанию, передается
// флаг `enable_alpha_plugins`
func Build(src string) ([]byte, error) {
	args := []string{"build"}
	args = append(args, kustomizeBuildFlags...)
	args = append(args, src)
	b, err := exec.Command("kustomize", args...).CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("error kustomize build: %v. output: %s", err, string(b))
	}
	// Если переменные окружения использовались в именах каких-то объектов, то делаем замену, чтобы это не вызвало
	// ошибок в каких-либо последующих преобразованиях
	result := os.ExpandEnv(string(b))
	return []byte(result), nil
}
