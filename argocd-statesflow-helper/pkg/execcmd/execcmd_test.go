package execcmd

import "testing"

func TestWithRetry(t *testing.T) {
	_, err := WithRetry("./", "foobar")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = WithRetry("./", "date")
	if err != nil {
		t.Error(err)
	}
}
