package execcmd

import (
	"os/exec"
	"time"
)

// WithRetry выполнить команду в указанном каталоге, если произошла
// ошибка – попробовать ещё раз (лимит в 3 попытки)
func WithRetry(dir, cmd string, arg ...string) ([]byte, error) {
	var b []byte
	var err error
	sleepyTime := time.Second
	for i := 0; i < 3; i++ {
		c := exec.Command(cmd, arg...)
		c.Dir = dir
		b, err = c.CombinedOutput()
		if err != nil {
			time.Sleep(sleepyTime)
			sleepyTime *= 2
			continue
		}
		break
	}
	return b, err
}
