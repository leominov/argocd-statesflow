package main

import (
	"os"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/command"

	"github.com/spf13/cobra"
)

const (
	rootHelp = `ArgoCD Statesflow Helper

Набор команд для работы с окружениями на базе двух репозиториев – репозиторий с Kustomize спецификациями и репозиторий
с состояниями окружений. Используется для запуска статичных и динамических тестовых окружений, и выступает, по большей
части, оберткой для нескольких инструментов.

При работе helper'а используются приложения:

  * argocd
  * kustomize 3.0
  * k8s-split
  * KustomizeProject 1.0
  * git

Их наличие обязательно для корректной работы, так же для работы потребуется ssh-клиент; чтобы убедиться, что окружение
соответствует всем требованиям, выполните команду:

  $ argocd-statesflow-helper validate

При создании приложения консольному клиенту ArgoCD будут переданы следующие флаги:

  * --path=resources
  * --sync-policy=automated
  * --self-heal=true
  * --auto-prune=true

Значения этих флагов, на текущий момент, нельзя переопределить через переменные окружения. Исходя из этого синхронизация
производится автоматически с помощью ArgoCD, при публикации изменений вызов процедуры синхронизация производиться не будет,
если вам кажется, что какие-то изменения не вступили в силу, проверьте статус синхронизации в интерфейсе ArgoCD или через
консольный клиент.`
)

var (
	rootCmd = &cobra.Command{
		Use:          "argocd-statesflow-helper",
		Long:         rootHelp,
		SilenceUsage: true,
	}
)

func init() {
	rootCmd.AddCommand(
		command.NewValidateCommand(),
		command.NewDestroyCommand(),
		command.NewCreateCommand(),
	)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
