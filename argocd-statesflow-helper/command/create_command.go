package command

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/envconfig"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/helper"
)

const (
	createHelp = `Создание окружения

При вызове команды происходит следующее:

  * обращение к Docker Registry для получение sha256 hash публикуемого образа;
  * клонирование репозиториев с Kustomize спецификациями и состояниями;
  * получение списка спецификаций приложения и его зависимостей;
  * фиксация изменений в репозитории состояний;
  * создание приложения в ArgoCD для синхронизации состояния с кластером.

Для гарантии работоспособности, перед выполнением, лучше провалидировать окружение командой:

  $ argocd-statesflow-helper validate`

	createError = `Операция была прервана по таймауту. Обратите внимание на возможные причины:

  * Операция действительно потребовала больше времени, чем предполагалось.
    Чтобы проверить, нужно перейти в интерфейс ArgoCD и посмотреть статус приложения,
	если оно запущено, значит все в порядке, если какие-то компоненты все еще
	инициализируются (например, происходит pull Docker-образа), просто дождитесь
	окончания операции;
  * Операция ожидания была прервана по таймауту из-за ошибок при запуске приложения.
    Чтобы проверить, нужно перейти в интерфейс ArgoCD и посмотреть статус
    сущностей публикуемого приложения, если в интерфейсе отражается проблема с
    запуском какого-либо Pod'а приложения, то переходим в его описание и смотрим
	журнал – вкладка Logs, чаще всего проблема сразу будет понятна. Если проблема
	связана с созданием служебных сущностей, например, ConfigMap или Service, то так
	же в интерфейсе переходим в описание и смотрим на сообщения на вкладке Events.

Если решить проблему самостоятельно не удалось, передайте информацию в чат с командой devops:

ArgoCD: %s
Окружение: %s
Ветка: %s (%s)`

	createSuccess = `Готово

Ссылка на окружение: %s
Ветка: %s (%s)`
)

var (
	createDryRun               bool
	createSkipWait             bool
	createAllowWaitFailure     bool
	createBranchPrefix         string
	createWaitResourceSelector string
)

// NewCreateCommand Создание окружения
func NewCreateCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create",
		Short: "Создание окружения",
		Long:  createHelp,
		RunE:  createCommandFunc,
	}
	cmd.Flags().BoolVar(&createDryRun, "dry-run", false, "запуск без создания приложения в ArgoCD")
	// При создании окружения для автотестов может возникнуть ситуация, когда ревью-стенд уже есть, в итоге имена веток
	// у двух приложений будут совпадать, но имена namespace – нет, при этом в репозитории будет описан только один
	// namespace – ревью-стенда
	cmd.Flags().StringVar(&createBranchPrefix, "branch-prefix", "", "префикс для имени ветки репозитория состояний (только для динамических окружений)")
	// На QA-окружениях часто бывают ситуации, что приложения работают нестабильно, из-за этого ожидание стабилизации
	// окружения становится невозможным
	cmd.Flags().BoolVar(&createSkipWait, "skip-wait", false, "пропустить ожидание запуска окружения")
	// Более мягкий вариант для `--skip-wait`, при котором мы ожидаем какое-то время, выводим информацию о таймауте, но
	// выходим с нулевым кодом
	cmd.Flags().BoolVar(&createAllowWaitFailure, "allow-wait-failure", false, "не возвращать ненулевой код в случае выхода по таймауту")
	cmd.Flags().StringVar(&createWaitResourceSelector, "wait-resource", "", "ожидать синхронизации определенного ресурса (в формате GROUP:KIND:NAME)")
	return cmd
}

func createCommandFunc(_ *cobra.Command, _ []string) error {
	opts := &helper.Options{
		BranchPrefix: createBranchPrefix,
	}

	err := envconfig.Process("", opts)
	if err != nil {
		return fmt.Errorf("ошибка настроек: %v", err)
	}

	h, err := helper.New(opts)
	if err != nil {
		return err
	}

	err = h.PrepareWorkspace()
	if err != nil {
		return err
	}

	// Нам все равно, если файлы, в результате работы, не будут удалены,
	// последующий запуск будет производиться в другом окружении, а рабочие
	// каталоги будут иметь другие имена
	defer h.Cleanup()

	fmt.Println("Получение репозиториев и подготовка спецификаций")
	err = h.FetchAll()
	if err != nil {
		return err
	}

	fmt.Println("Обновление референса образа приложения")
	_, err = h.ReplaceImageDigest()
	if err != nil {
		return err
	}

	fmt.Println("Публикация изменений")
	err = h.PushChanges()
	if err != nil {
		return err
	}

	if createDryRun {
		fmt.Printf("Готово. Шаг с созданием %q окружения пропущен из-за флага `--dry-run`\n", opts.Environment())
		return nil
	}

	fmt.Printf("Создание %q окружения\n", opts.Environment())
	err = h.CreateEnvironment()
	if err != nil {
		return err
	}

	if !createSkipWait {
		fmt.Println("Ожидание запуска (это может занять несколько минут)...")
		if !h.WaitEnvironment(createWaitResourceSelector) {
			message := fmt.Sprintf(
				createError,
				opts.ApplicationURL(),
				opts.Environment(),
				opts.Branch(),
				opts.StatesRepo,
			)
			if createAllowWaitFailure {
				fmt.Println(message)
				return nil
			}
			return errors.New(message)
		}
	}

	fmt.Printf(
		createSuccess,
		opts.GitLab.Environment.URL,
		opts.Branch(),
		opts.StatesRepo,
	)

	return nil
}
