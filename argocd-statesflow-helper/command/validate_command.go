package command

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/argocd"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/envconfig"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/helper"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/registry"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/vault"
)

type check func(opts *helper.Options) bool

const (
	validateLong = `Валидация окружения

При вызове будет проверено наличие необходимых переменных окружения и инструментов, которые используются при запуске
или остановке окружения.

Переменные можно разделить на несколько групп:

  * с префиксом "CI_" и "GITLAB_" – задаются пользователем из ".gitlab-ci.yml" или передаются автоматически из GitLab;
  * с префиксом "ARGOCD_" – задаются пользователем через переменные окружения, необходимо для обращения к ArgoCD;
  * с префиксом "VAULT_" – задаются пользователем через переменные окружения, необходимо для обращения к Vault;
  * с префиксом "STATESFLOW_" – задаются пользователем и используются только в argocd-statesflow-helper.`

	listFormat = `Не заданы следующие переменные окружения:{{range .}}
{{- if eq (usage_exists .) false}}
  {{usage_key .}} ({{usage_type .}})
    описание:     {{usage_description .}}
    источник:     {{usage_source .}}{{end}}{{end}}
`
)

var (
	tools = []helper.Tool{
		{
			Path:   "kustomize",
			Reason: "Получение итоговых спецификаций",
		},
		{
			Path:   "$HOME/.config/kustomize/plugin/advert.tinkoff.ru/v1/kustomizeproject/KustomizeProject",
			Reason: "Плагин для Kustomize",
		},
		{
			Path:   "argocd",
			Reason: "Обращение с сервером ArgoCD",
		},
		{
			Path:   "k8s-split",
			Reason: "Разделение YAML-мультидокумента на файлы",
		},
		{
			Path:   "git",
			Reason: "Работа с репозиториями",
		},
		{
			Path:   "ssh-keyscan",
			Reason: "Для получения SSH публичных ключей GitLab",
		},
	}
)

// NewValidateCommand валидация окружения
func NewValidateCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "validate",
		Short: "Валидация окружения",
		Long:  validateLong,
		RunE:  validateCommandFunc,
	}
	return cmd
}

func validateCommandFunc(_ *cobra.Command, _ []string) error {
	var failed bool
	opts := &helper.Options{}
	checks := []check{
		checkVariables,
		checkTools,
		checkArgoCD,
		checkVault,
		checkDockerRegistry,
	}
	for _, c := range checks {
		if ok := c(opts); !ok {
			failed = true
		}
	}
	if failed {
		return errors.New("проверка провалена")
	}
	return nil
}

func checkVariables(opts *helper.Options) bool {
	fmt.Println("Проверка необходимых переменных окружения")
	if err := envconfig.Process("", opts); err != nil {
		envconfig.Usagef("", opts, os.Stderr, listFormat)
		return false
	}
	return true
}

func checkTools(_ *helper.Options) bool {
	var failed bool
	fmt.Println("Проверка необходимых инструментов")
	for _, tool := range tools {
		if tool.Exists() {
			continue
		}
		failed = true
		fmt.Printf("  %s – %s\n", tool.Path, tool.Reason)
	}
	return !failed
}

func checkArgoCD(opts *helper.Options) bool {
	fmt.Println("Проверка ArgoCD")
	err := argocd.Check()
	if err != nil {
		fmt.Println(err)
		return false
	}
	ok, err := argocd.RepoExists(opts.StatesRepo)
	if err != nil {
		fmt.Printf("Ошибка при обращении к ArgoCD: %v\n", err)
		return false
	}
	if !ok {
		fmt.Printf("Репозиторий %s отсуствует в ArgoCD\n", opts.StatesRepo)
		return false
	}
	return true
}

func checkVault(opts *helper.Options) bool {
	fmt.Println("Проверка Vault")
	_, err := vault.New(&opts.Vault)
	if err != nil {
		fmt.Printf("Ошибка при обращении к Vault: %v\n", err)
		return false
	}
	return true
}

func checkDockerRegistry(opts *helper.Options) bool {
	// via docker config
	key, _ := registry.DockerRegistryKeyFromConfig(opts.DockerImage, opts.DockerAuthConfig)
	if len(key) != 0 {
		fmt.Println("Проверка Docker Registry (Docker Config)")
		_, err := (registry.New(key)).Digest(opts.DockerImage)
		if err != nil {
			fmt.Printf("Ошибка при обращении к Docker Registry: %v\n", err)
			return false
		}
		return true
	}

	// via vault
	fmt.Println("Проверка Docker Registry (Vault)")
	v, err := vault.New(&opts.Vault)
	if err != nil {
		fmt.Printf("Ошибка при обращении к Vault: %v\n", err)
		return false
	}
	key, err = v.ReadByPath(opts.GCloudKey)
	if err != nil {
		fmt.Printf("Ошибка получения ключа доступа к Docker Registry: %v\n", err)
		return false
	}
	_, err = (registry.New(key)).Digest(opts.DockerImage)
	if err != nil {
		fmt.Printf("Ошибка при обращении к Docker Registry: %v\n", err)
		return false
	}

	return true
}
