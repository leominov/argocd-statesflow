package command

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/envconfig"
	"gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper/pkg/helper"
)

const (
	destroyHelp = `Удаление окружения

При вызове будет удалено приложение из ArgoCD. Команда использует только клиент ArgoCD, для гарантии работоспособности,
перед выполнением, лучше провалидировать окружение командой:

  $ argocd-statesflow-helper validate`

	destroyError = `Не удалось удалить окружение, передайте информацию в чат с командой devops:

ArgoCD: %s
Окружение: %s
Ветка: %s (%s)
Ошибка: %v`
)

var (
	destroyBranchPrefix string
)

// NewDestroyCommand Остановка окружения
func NewDestroyCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "destroy",
		Short: "Удаление окружения",
		Long:  destroyHelp,
		RunE:  destroyCommandFunc,
	}
	// Смотри описание в NewCreateCommand
	cmd.Flags().StringVar(&destroyBranchPrefix, "branch-prefix", "", "префикс для имени ветки репозитория состояний (только для динамических окружений)")
	return cmd
}

func destroyCommandFunc(_ *cobra.Command, _ []string) error {
	opts := &helper.Options{
		BranchPrefix: destroyBranchPrefix,
	}

	// Логичнее было бы разделить настройки на те, что нужны для создания и те,
	// что нужны для удаления окружения, так как неиспользуемые переменные все
	// равно будут обязательны для передачи
	err := envconfig.Process("", opts)
	if err != nil {
		return fmt.Errorf("ошибка настроек: %v", err)
	}

	h, err := helper.New(opts)
	if err != nil {
		return err
	}

	fmt.Printf("Удаление %q окружения...\n", opts.Environment())
	err = h.DestroyEnvironment()
	if err != nil {
		return fmt.Errorf(
			destroyError,
			opts.ApplicationURL(),
			opts.Environment(),
			opts.Branch(),
			opts.StatesRepo,
			err,
		)
	}

	fmt.Println("Готово")

	return nil
}
