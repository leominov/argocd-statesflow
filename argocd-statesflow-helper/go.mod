module gitlab-rnd.tcsbank.ru/devops/argocd-statesflow/argocd-statesflow-helper

go 1.13

require (
	github.com/google/go-containerregistry v0.0.0-20191206185556-eb7c14b719c6
	github.com/hashicorp/vault/api v1.0.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.4.0
)
